﻿namespace Task17UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.CharacterSummaryLabel = new System.Windows.Forms.Label();
            this.MainCharacterAttrTableLay = new System.Windows.Forms.TableLayoutPanel();
            this.ManaTextBox = new System.Windows.Forms.TextBox();
            this.ManaLabel = new System.Windows.Forms.Label();
            this.HPTextBox = new System.Windows.Forms.TextBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.HPLabel = new System.Windows.Forms.Label();
            this.ClassGroupBox = new System.Windows.Forms.GroupBox();
            this.ThiefRadioButton = new System.Windows.Forms.RadioButton();
            this.WarriorRadioButton = new System.Windows.Forms.RadioButton();
            this.WizardRadioButton = new System.Windows.Forms.RadioButton();
            this.CharacterAttributesTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.DaggerLabel = new System.Windows.Forms.Label();
            this.DaggerCheckBox = new System.Windows.Forms.CheckBox();
            this.ShieldLabel = new System.Windows.Forms.Label();
            this.ArmorTextBox = new System.Windows.Forms.TextBox();
            this.ArmorLabel = new System.Windows.Forms.Label();
            this.ShieldCheckBox = new System.Windows.Forms.CheckBox();
            this.WizardSpecializationGroupBox = new System.Windows.Forms.GroupBox();
            this.ArcaneSpecializationRadioButton = new System.Windows.Forms.RadioButton();
            this.FrostSpecializationRadioButton = new System.Windows.Forms.RadioButton();
            this.FireSpecializationRadioButton = new System.Windows.Forms.RadioButton();
            this.CharacterSummaryRichTextBox = new System.Windows.Forms.RichTextBox();
            this.CreateButton = new System.Windows.Forms.Button();
            this.DBCharactersListView = new System.Windows.Forms.ListView();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.GetCharactersButton = new System.Windows.Forms.Button();
            this.MainCharacterAttrTableLay.SuspendLayout();
            this.ClassGroupBox.SuspendLayout();
            this.CharacterAttributesTableLayoutPanel.SuspendLayout();
            this.WizardSpecializationGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Choose character";
            // 
            // CharacterSummaryLabel
            // 
            this.CharacterSummaryLabel.AutoSize = true;
            this.CharacterSummaryLabel.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CharacterSummaryLabel.Location = new System.Drawing.Point(310, 41);
            this.CharacterSummaryLabel.Name = "CharacterSummaryLabel";
            this.CharacterSummaryLabel.Size = new System.Drawing.Size(147, 20);
            this.CharacterSummaryLabel.TabIndex = 7;
            this.CharacterSummaryLabel.Text = "Character summary";
            this.CharacterSummaryLabel.Visible = false;
            // 
            // MainCharacterAttrTableLay
            // 
            this.MainCharacterAttrTableLay.ColumnCount = 2;
            this.MainCharacterAttrTableLay.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.79336F));
            this.MainCharacterAttrTableLay.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.20664F));
            this.MainCharacterAttrTableLay.Controls.Add(this.ManaTextBox, 1, 2);
            this.MainCharacterAttrTableLay.Controls.Add(this.ManaLabel, 0, 2);
            this.MainCharacterAttrTableLay.Controls.Add(this.HPTextBox, 1, 1);
            this.MainCharacterAttrTableLay.Controls.Add(this.NameLabel, 0, 0);
            this.MainCharacterAttrTableLay.Controls.Add(this.NameTextBox, 1, 0);
            this.MainCharacterAttrTableLay.Controls.Add(this.HPLabel, 0, 1);
            this.MainCharacterAttrTableLay.Location = new System.Drawing.Point(29, 78);
            this.MainCharacterAttrTableLay.Name = "MainCharacterAttrTableLay";
            this.MainCharacterAttrTableLay.RowCount = 3;
            this.MainCharacterAttrTableLay.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MainCharacterAttrTableLay.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MainCharacterAttrTableLay.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.MainCharacterAttrTableLay.Size = new System.Drawing.Size(271, 173);
            this.MainCharacterAttrTableLay.TabIndex = 8;
            this.MainCharacterAttrTableLay.Visible = false;
            // 
            // ManaTextBox
            // 
            this.ManaTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ManaTextBox.Location = new System.Drawing.Point(100, 144);
            this.ManaTextBox.Name = "ManaTextBox";
            this.ManaTextBox.Size = new System.Drawing.Size(168, 26);
            this.ManaTextBox.TabIndex = 11;
            this.ManaTextBox.TextChanged += new System.EventHandler(this.ManaTextBox_TextChanged);
            // 
            // ManaLabel
            // 
            this.ManaLabel.AutoSize = true;
            this.ManaLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ManaLabel.Location = new System.Drawing.Point(3, 108);
            this.ManaLabel.Name = "ManaLabel";
            this.ManaLabel.Size = new System.Drawing.Size(91, 65);
            this.ManaLabel.TabIndex = 11;
            this.ManaLabel.Text = "Mana";
            this.ManaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HPTextBox
            // 
            this.HPTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.HPTextBox.Location = new System.Drawing.Point(100, 79);
            this.HPTextBox.Name = "HPTextBox";
            this.HPTextBox.Size = new System.Drawing.Size(168, 26);
            this.HPTextBox.TabIndex = 10;
            this.HPTextBox.TextChanged += new System.EventHandler(this.HPTextBox_TextChanged);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NameLabel.Location = new System.Drawing.Point(3, 0);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(91, 54);
            this.NameLabel.TabIndex = 9;
            this.NameLabel.Text = "Name";
            this.NameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.NameTextBox.Location = new System.Drawing.Point(100, 25);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(168, 26);
            this.NameTextBox.TabIndex = 9;
            this.NameTextBox.TextChanged += new System.EventHandler(this.NameTextBox_TextChanged);
            // 
            // HPLabel
            // 
            this.HPLabel.AutoSize = true;
            this.HPLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HPLabel.Location = new System.Drawing.Point(3, 54);
            this.HPLabel.Name = "HPLabel";
            this.HPLabel.Size = new System.Drawing.Size(91, 54);
            this.HPLabel.TabIndex = 10;
            this.HPLabel.Text = "HP";
            this.HPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ClassGroupBox
            // 
            this.ClassGroupBox.Controls.Add(this.ThiefRadioButton);
            this.ClassGroupBox.Controls.Add(this.WarriorRadioButton);
            this.ClassGroupBox.Controls.Add(this.WizardRadioButton);
            this.ClassGroupBox.Location = new System.Drawing.Point(29, 258);
            this.ClassGroupBox.Name = "ClassGroupBox";
            this.ClassGroupBox.Size = new System.Drawing.Size(137, 133);
            this.ClassGroupBox.TabIndex = 9;
            this.ClassGroupBox.TabStop = false;
            this.ClassGroupBox.Text = "Class";
            // 
            // ThiefRadioButton
            // 
            this.ThiefRadioButton.AutoSize = true;
            this.ThiefRadioButton.Location = new System.Drawing.Point(6, 94);
            this.ThiefRadioButton.Name = "ThiefRadioButton";
            this.ThiefRadioButton.Size = new System.Drawing.Size(69, 24);
            this.ThiefRadioButton.TabIndex = 12;
            this.ThiefRadioButton.TabStop = true;
            this.ThiefRadioButton.Text = "Thief";
            this.ThiefRadioButton.UseVisualStyleBackColor = true;
            this.ThiefRadioButton.CheckedChanged += new System.EventHandler(this.ThiefRadioButton_CheckedChanged);
            // 
            // WarriorRadioButton
            // 
            this.WarriorRadioButton.AutoSize = true;
            this.WarriorRadioButton.Location = new System.Drawing.Point(6, 34);
            this.WarriorRadioButton.Name = "WarriorRadioButton";
            this.WarriorRadioButton.Size = new System.Drawing.Size(85, 24);
            this.WarriorRadioButton.TabIndex = 10;
            this.WarriorRadioButton.TabStop = true;
            this.WarriorRadioButton.Text = "Warrior";
            this.WarriorRadioButton.UseVisualStyleBackColor = true;
            this.WarriorRadioButton.CheckedChanged += new System.EventHandler(this.WarriorRadioButton_CheckedChanged);
            // 
            // WizardRadioButton
            // 
            this.WizardRadioButton.AutoSize = true;
            this.WizardRadioButton.Location = new System.Drawing.Point(5, 64);
            this.WizardRadioButton.Name = "WizardRadioButton";
            this.WizardRadioButton.Size = new System.Drawing.Size(83, 24);
            this.WizardRadioButton.TabIndex = 11;
            this.WizardRadioButton.TabStop = true;
            this.WizardRadioButton.Text = "Wizard";
            this.WizardRadioButton.UseVisualStyleBackColor = true;
            this.WizardRadioButton.CheckedChanged += new System.EventHandler(this.WizardRadioButton_CheckedChanged);
            // 
            // CharacterAttributesTableLayoutPanel
            // 
            this.CharacterAttributesTableLayoutPanel.ColumnCount = 2;
            this.CharacterAttributesTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37F));
            this.CharacterAttributesTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63F));
            this.CharacterAttributesTableLayoutPanel.Controls.Add(this.DaggerLabel, 0, 2);
            this.CharacterAttributesTableLayoutPanel.Controls.Add(this.DaggerCheckBox, 1, 2);
            this.CharacterAttributesTableLayoutPanel.Controls.Add(this.ShieldLabel, 0, 1);
            this.CharacterAttributesTableLayoutPanel.Controls.Add(this.ArmorTextBox, 1, 0);
            this.CharacterAttributesTableLayoutPanel.Controls.Add(this.ArmorLabel, 0, 0);
            this.CharacterAttributesTableLayoutPanel.Controls.Add(this.ShieldCheckBox, 1, 1);
            this.CharacterAttributesTableLayoutPanel.Location = new System.Drawing.Point(188, 258);
            this.CharacterAttributesTableLayoutPanel.Name = "CharacterAttributesTableLayoutPanel";
            this.CharacterAttributesTableLayoutPanel.RowCount = 3;
            this.CharacterAttributesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.CharacterAttributesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.CharacterAttributesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.CharacterAttributesTableLayoutPanel.Size = new System.Drawing.Size(200, 133);
            this.CharacterAttributesTableLayoutPanel.TabIndex = 10;
            this.CharacterAttributesTableLayoutPanel.Visible = false;
            // 
            // DaggerLabel
            // 
            this.DaggerLabel.AutoSize = true;
            this.DaggerLabel.Location = new System.Drawing.Point(3, 98);
            this.DaggerLabel.Name = "DaggerLabel";
            this.DaggerLabel.Size = new System.Drawing.Size(62, 20);
            this.DaggerLabel.TabIndex = 11;
            this.DaggerLabel.Text = "Dagger";
            this.DaggerLabel.Visible = false;
            // 
            // DaggerCheckBox
            // 
            this.DaggerCheckBox.AutoSize = true;
            this.DaggerCheckBox.Location = new System.Drawing.Point(77, 101);
            this.DaggerCheckBox.Name = "DaggerCheckBox";
            this.DaggerCheckBox.Size = new System.Drawing.Size(22, 21);
            this.DaggerCheckBox.TabIndex = 11;
            this.DaggerCheckBox.UseVisualStyleBackColor = true;
            this.DaggerCheckBox.Visible = false;
            this.DaggerCheckBox.CheckedChanged += new System.EventHandler(this.DaggerCheckBox_CheckedChanged);
            // 
            // ShieldLabel
            // 
            this.ShieldLabel.AutoSize = true;
            this.ShieldLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShieldLabel.Location = new System.Drawing.Point(3, 49);
            this.ShieldLabel.Name = "ShieldLabel";
            this.ShieldLabel.Size = new System.Drawing.Size(68, 49);
            this.ShieldLabel.TabIndex = 11;
            this.ShieldLabel.Text = "Shield";
            this.ShieldLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ArmorTextBox
            // 
            this.ArmorTextBox.Location = new System.Drawing.Point(77, 3);
            this.ArmorTextBox.Name = "ArmorTextBox";
            this.ArmorTextBox.Size = new System.Drawing.Size(100, 26);
            this.ArmorTextBox.TabIndex = 11;
            this.ArmorTextBox.TextChanged += new System.EventHandler(this.ArmorTextBox_TextChanged);
            // 
            // ArmorLabel
            // 
            this.ArmorLabel.AutoSize = true;
            this.ArmorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ArmorLabel.Location = new System.Drawing.Point(3, 0);
            this.ArmorLabel.Name = "ArmorLabel";
            this.ArmorLabel.Size = new System.Drawing.Size(68, 49);
            this.ArmorLabel.TabIndex = 11;
            this.ArmorLabel.Text = "Armor";
            this.ArmorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ShieldCheckBox
            // 
            this.ShieldCheckBox.AutoSize = true;
            this.ShieldCheckBox.Location = new System.Drawing.Point(77, 52);
            this.ShieldCheckBox.Name = "ShieldCheckBox";
            this.ShieldCheckBox.Size = new System.Drawing.Size(22, 21);
            this.ShieldCheckBox.TabIndex = 12;
            this.ShieldCheckBox.UseVisualStyleBackColor = true;
            this.ShieldCheckBox.CheckedChanged += new System.EventHandler(this.ShieldCheckBox_CheckedChanged);
            // 
            // WizardSpecializationGroupBox
            // 
            this.WizardSpecializationGroupBox.Controls.Add(this.ArcaneSpecializationRadioButton);
            this.WizardSpecializationGroupBox.Controls.Add(this.FrostSpecializationRadioButton);
            this.WizardSpecializationGroupBox.Controls.Add(this.FireSpecializationRadioButton);
            this.WizardSpecializationGroupBox.Location = new System.Drawing.Point(405, 258);
            this.WizardSpecializationGroupBox.Name = "WizardSpecializationGroupBox";
            this.WizardSpecializationGroupBox.Size = new System.Drawing.Size(200, 133);
            this.WizardSpecializationGroupBox.TabIndex = 11;
            this.WizardSpecializationGroupBox.TabStop = false;
            this.WizardSpecializationGroupBox.Text = "Specialization";
            this.WizardSpecializationGroupBox.Visible = false;
            // 
            // ArcaneSpecializationRadioButton
            // 
            this.ArcaneSpecializationRadioButton.AutoSize = true;
            this.ArcaneSpecializationRadioButton.Location = new System.Drawing.Point(20, 93);
            this.ArcaneSpecializationRadioButton.Name = "ArcaneSpecializationRadioButton";
            this.ArcaneSpecializationRadioButton.Size = new System.Drawing.Size(85, 24);
            this.ArcaneSpecializationRadioButton.TabIndex = 2;
            this.ArcaneSpecializationRadioButton.TabStop = true;
            this.ArcaneSpecializationRadioButton.Text = "Arcane";
            this.ArcaneSpecializationRadioButton.UseVisualStyleBackColor = true;
            this.ArcaneSpecializationRadioButton.CheckedChanged += new System.EventHandler(this.ArcaneSpecializationRadioButton_CheckedChanged);
            // 
            // FrostSpecializationRadioButton
            // 
            this.FrostSpecializationRadioButton.AutoSize = true;
            this.FrostSpecializationRadioButton.Location = new System.Drawing.Point(20, 64);
            this.FrostSpecializationRadioButton.Name = "FrostSpecializationRadioButton";
            this.FrostSpecializationRadioButton.Size = new System.Drawing.Size(71, 24);
            this.FrostSpecializationRadioButton.TabIndex = 1;
            this.FrostSpecializationRadioButton.TabStop = true;
            this.FrostSpecializationRadioButton.Text = "Frost";
            this.FrostSpecializationRadioButton.UseVisualStyleBackColor = true;
            this.FrostSpecializationRadioButton.CheckedChanged += new System.EventHandler(this.FrostSpecializationRadioButton_CheckedChanged);
            // 
            // FireSpecializationRadioButton
            // 
            this.FireSpecializationRadioButton.AutoSize = true;
            this.FireSpecializationRadioButton.Location = new System.Drawing.Point(20, 33);
            this.FireSpecializationRadioButton.Name = "FireSpecializationRadioButton";
            this.FireSpecializationRadioButton.Size = new System.Drawing.Size(61, 24);
            this.FireSpecializationRadioButton.TabIndex = 0;
            this.FireSpecializationRadioButton.TabStop = true;
            this.FireSpecializationRadioButton.Text = "Fire";
            this.FireSpecializationRadioButton.UseVisualStyleBackColor = true;
            this.FireSpecializationRadioButton.CheckedChanged += new System.EventHandler(this.FireSpecializationRadioButton_CheckedChanged);
            // 
            // CharacterSummaryRichTextBox
            // 
            this.CharacterSummaryRichTextBox.Location = new System.Drawing.Point(314, 78);
            this.CharacterSummaryRichTextBox.Name = "CharacterSummaryRichTextBox";
            this.CharacterSummaryRichTextBox.Size = new System.Drawing.Size(160, 155);
            this.CharacterSummaryRichTextBox.TabIndex = 12;
            this.CharacterSummaryRichTextBox.Text = "";
            this.CharacterSummaryRichTextBox.Visible = false;
            // 
            // CreateButton
            // 
            this.CreateButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.CreateButton.Location = new System.Drawing.Point(626, 331);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(149, 60);
            this.CreateButton.TabIndex = 13;
            this.CreateButton.Text = "Create";
            this.CreateButton.UseVisualStyleBackColor = false;
            this.CreateButton.Visible = false;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // DBCharactersListView
            // 
            this.DBCharactersListView.Location = new System.Drawing.Point(480, 78);
            this.DBCharactersListView.Name = "DBCharactersListView";
            this.DBCharactersListView.Size = new System.Drawing.Size(145, 155);
            this.DBCharactersListView.TabIndex = 14;
            this.DBCharactersListView.UseCompatibleStateImageBehavior = false;
            this.DBCharactersListView.View = System.Windows.Forms.View.SmallIcon;
            this.DBCharactersListView.ItemActivate += new System.EventHandler(this.DBCharactersListView_ItemActivate);
            // 
            // DeleteButton
            // 
            this.DeleteButton.BackColor = System.Drawing.Color.OrangeRed;
            this.DeleteButton.Location = new System.Drawing.Point(658, 78);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(102, 38);
            this.DeleteButton.TabIndex = 15;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = false;
            this.DeleteButton.Visible = false;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // UpdateButton
            // 
            this.UpdateButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.UpdateButton.Location = new System.Drawing.Point(658, 123);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(102, 32);
            this.UpdateButton.TabIndex = 16;
            this.UpdateButton.Text = "Update";
            this.UpdateButton.UseVisualStyleBackColor = false;
            this.UpdateButton.Visible = false;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // GetCharactersButton
            // 
            this.GetCharactersButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.GetCharactersButton.Location = new System.Drawing.Point(626, 283);
            this.GetCharactersButton.Name = "GetCharactersButton";
            this.GetCharactersButton.Size = new System.Drawing.Size(149, 42);
            this.GetCharactersButton.TabIndex = 17;
            this.GetCharactersButton.Text = "Get characters";
            this.GetCharactersButton.UseVisualStyleBackColor = false;
            this.GetCharactersButton.Click += new System.EventHandler(this.GetCharactersButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GetCharactersButton);
            this.Controls.Add(this.UpdateButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.DBCharactersListView);
            this.Controls.Add(this.CreateButton);
            this.Controls.Add(this.CharacterSummaryRichTextBox);
            this.Controls.Add(this.WizardSpecializationGroupBox);
            this.Controls.Add(this.CharacterAttributesTableLayoutPanel);
            this.Controls.Add(this.ClassGroupBox);
            this.Controls.Add(this.MainCharacterAttrTableLay);
            this.Controls.Add(this.CharacterSummaryLabel);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MainCharacterAttrTableLay.ResumeLayout(false);
            this.MainCharacterAttrTableLay.PerformLayout();
            this.ClassGroupBox.ResumeLayout(false);
            this.ClassGroupBox.PerformLayout();
            this.CharacterAttributesTableLayoutPanel.ResumeLayout(false);
            this.CharacterAttributesTableLayoutPanel.PerformLayout();
            this.WizardSpecializationGroupBox.ResumeLayout(false);
            this.WizardSpecializationGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label CharacterSummaryLabel;
        private System.Windows.Forms.TableLayoutPanel MainCharacterAttrTableLay;
        private System.Windows.Forms.Label ManaLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label HPLabel;
        private System.Windows.Forms.TextBox ManaTextBox;
        private System.Windows.Forms.TextBox HPTextBox;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.GroupBox ClassGroupBox;
        private System.Windows.Forms.RadioButton ThiefRadioButton;
        private System.Windows.Forms.RadioButton WarriorRadioButton;
        private System.Windows.Forms.RadioButton WizardRadioButton;
        private System.Windows.Forms.TableLayoutPanel CharacterAttributesTableLayoutPanel;
        private System.Windows.Forms.Label ArmorLabel;
        private System.Windows.Forms.TextBox ArmorTextBox;
        private System.Windows.Forms.CheckBox ShieldCheckBox;
        private System.Windows.Forms.Label ShieldLabel;
        private System.Windows.Forms.CheckBox DaggerCheckBox;
        private System.Windows.Forms.Label DaggerLabel;
        private System.Windows.Forms.GroupBox WizardSpecializationGroupBox;
        private System.Windows.Forms.RadioButton ArcaneSpecializationRadioButton;
        private System.Windows.Forms.RadioButton FrostSpecializationRadioButton;
        private System.Windows.Forms.RadioButton FireSpecializationRadioButton;
        private System.Windows.Forms.RichTextBox CharacterSummaryRichTextBox;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.ListView DBCharactersListView;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Button GetCharactersButton;
    }
}

