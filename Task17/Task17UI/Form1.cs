﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Task17;

namespace Task17UI
{
    public partial class Form1 : Form
    {
        private DatabaseAccessor databaseAccessor;

        private Character chosenCharacter;

        //character attributes
        private string name;
        private int hp;
        private int mana;
        //
        private int armor;
        private bool hasShield;
        //thief attributes
        private bool hasDagger;
        //wizard attributes
        private Character.WizardSpecialization wizardSpecialization;
        //string variant
        private string wizardSpecializationToString = null;

        //chosen class
        private string chosenClass;

        //selected characterItem
        private Character selectedCharacterItem = null;

        public Form1()
        {
            this.databaseAccessor = new DatabaseAccessor();
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void ShieldCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            //set currentCharacter shield true
            this.hasShield = !this.hasShield;

        }

        private void WarriorRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MainCharacterAttrTableLay.Visible = true;
            CharacterAttributesTableLayoutPanel.Visible = true;
            //set dagger option not visible
            DaggerLabel.Visible = false;
            DaggerCheckBox.Visible = false;
            //set mana option not visible
            ManaLabel.Visible = false;
            ManaTextBox.Visible = false;
            //set wizardSpecialization group not visible
            WizardSpecializationGroupBox.Visible = false;

            //set currentCharacter to warrior
            this.chosenClass = "Warrior";

            //set create button visible
            CreateButton.Visible = true;
        }

        private void WizardRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MainCharacterAttrTableLay.Visible = true;
            CharacterAttributesTableLayoutPanel.Visible = true;
            //set dagger option not visible
            DaggerLabel.Visible = false;
            DaggerCheckBox.Visible = false;
            //set mana option visible
            ManaLabel.Visible = true;
            ManaTextBox.Visible = true;
            //set wizardSpecialization group visible
            WizardSpecializationGroupBox.Visible = true;

            //set currentCharacter to wizard
            this.chosenClass = "Wizard";

            //set create button visible
            CreateButton.Visible = false;

        }

        private void ThiefRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MainCharacterAttrTableLay.Visible = true;
            CharacterAttributesTableLayoutPanel.Visible = true;
            //set dagger option visible
            DaggerLabel.Visible = true;
            DaggerCheckBox.Visible = true;
            //set mana option visible
            ManaLabel.Visible = true;
            ManaTextBox.Visible = true;
            //set wizardSpecialization group not visible
            WizardSpecializationGroupBox.Visible = false;

            //set currentCharacter to thief
            this.chosenClass = "Thief";

            //set create button visible
            CreateButton.Visible = true;

        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            //set the currentCharacter
            if (this.chosenClass == "Warrior")
            {
                this.chosenCharacter = new Warrior(this.hp, this.name, this.hasShield, this.armor);

            }
            else if (this.chosenClass == "Thief")
            {
                Thief thief = new Thief(this.hp, this.name, this.hasDagger, this.armor);
                thief.Energy = this.mana;
                this.chosenCharacter = thief;
            }
            else
            {
                Wizard wizard = new Wizard(this.hp, this.name, this.wizardSpecialization, this.armor);
                wizard.Mana = this.mana;
                this.chosenCharacter = wizard;
            }
            //set summary textBox visible
            CharacterSummaryRichTextBox.Visible = true;

            //display currentCharacter to summary textBox
            CharacterSummaryLabel.Visible = true;
            CharacterSummaryRichTextBox.Text = this.chosenCharacter.ToString();

            //convert hasShield, hasDagger bool to int for Db
            int shield = 0;
            int dagger = 0;

            if (this.hasShield == true)
            {
                shield = 1;
            }
            if (this.hasDagger == true)
            {
                dagger = 1;
            }

            //store to file
            this.databaseAccessor.Insert(
                this.chosenClass,
                this.name,
                this.hp,
                shield,
                this.mana,
                dagger,
                this.wizardSpecializationToString
                );
        }

        private string handleWizardSpecializationVariants()
        {
            if (this.wizardSpecializationToString != null)
            {
                return (char)34 + this.wizardSpecializationToString + (char)34;
            }
            else
            {
                return null;
            }
        }

        private void NameTextBox_TextChanged(object sender, EventArgs e)
        {
            this.name = NameTextBox.Text;
        }

        private void HPTextBox_TextChanged(object sender, EventArgs e)
        {
            if (this.selectedCharacterItem != null)
            {
                this.selectedCharacterItem.Hp = int.Parse(HPTextBox.Text);
            }
            else
            {
                this.hp = int.Parse(HPTextBox.Text);
            }
        }

        private void ManaTextBox_TextChanged(object sender, EventArgs e)
        {
            this.mana = int.Parse(ManaTextBox.Text);
        }

        private void ArmorTextBox_TextChanged(object sender, EventArgs e)
        {
            this.armor = int.Parse(ArmorTextBox.Text);
        }

        private void DaggerCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.hasDagger = !this.hasDagger;
        }

        private void FireSpecializationRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.wizardSpecialization = Character.WizardSpecialization.Fire;
            //set string variant
            this.wizardSpecializationToString = "Fire";

            //set create button visible
            CreateButton.Visible = true;
        }

        private void FrostSpecializationRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.wizardSpecialization = Character.WizardSpecialization.Frost;
            //set string variant
            this.wizardSpecializationToString = "Frost";

            //set create button visible
            CreateButton.Visible = true;
        }

        private void ArcaneSpecializationRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.wizardSpecialization = Character.WizardSpecialization.Arcane;
            //set string variant
            this.wizardSpecializationToString = "Arcane";

            //set create button visible
            CreateButton.Visible = true;
        }

        private void GetCharactersButton_Click(object sender, EventArgs e)
        {
            DBCharactersListView.Clear();
            List<Character> characters = new List<Character>();

            characters = this.databaseAccessor.GetCharacters();



            foreach (Character character in characters)
            {
                DBCharactersListView.Items.Add(character.Name);
            }

            //set create button not visible
            CreateButton.Visible = false;
            //class and different attributes not visible
            ClassGroupBox.Visible = false;
            CharacterAttributesTableLayoutPanel.Visible = false;
            WizardSpecializationGroupBox.Visible = false;
            ManaLabel.Visible = false;
            ManaTextBox.Visible = false;
            NameLabel.Visible = false;
            NameTextBox.Visible = false;

        }

        private void DBCharactersListView_ItemActivate(object sender, EventArgs e)
        {
            //set selectedCharacterItem to = Character get(name)
            this.selectedCharacterItem = this.databaseAccessor.Get(DBCharactersListView.SelectedItems[0].Text);
            //display character.ToString in CharacterSummaryTextBox
            CharacterSummaryRichTextBox.Visible = true;
            CharacterSummaryRichTextBox.Text = this.selectedCharacterItem.ToString();
            //display update and delete options
            UpdateButton.Visible = true;
            DeleteButton.Visible = true;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            databaseAccessor.Delete(this.selectedCharacterItem.Name);
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            databaseAccessor.Update(this.selectedCharacterItem);
        }
    }
}
