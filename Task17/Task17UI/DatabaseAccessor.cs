﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using Task17;

namespace Task17UI
{
    public class DatabaseAccessor
    {
        private SQLiteConnection connection;
        private string projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

        public DatabaseAccessor()
        {
            this.connection = CreateConnection();
        }

        public SQLiteConnection Connection { get => connection; set => connection = value; }
        public string ProjectDir { get => projectDir; set => projectDir = value; }

        public SQLiteConnection CreateConnection()
        {
            SQLiteConnection sqlite_conn;

            // Create a new database connection:
            sqlite_conn = new SQLiteConnection($"Data Source={projectDir}\\CharacterDatabase.db; Version = 3; New = True; Compress = True; ");

            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return sqlite_conn;
        }

        //has to specify attributes separately because of inadequate class inheritance
        public void Insert(
            string chosenClass,
            string name,
            int hp,
            int hasShield,
            int mana,
            int hasDagger,
            string wizardSpec
            )
        {
            //create random ID
            int randomId = new Random().Next(100);

            SQLiteCommand sqlite_cmd;

            sqlite_cmd = this.connection.CreateCommand();

            if (chosenClass == "Wizard")
            {
                sqlite_cmd.CommandText =
                     "INSERT INTO Characters("
                    + "Id, "
                    + "Class, "
                    + "Name, "
                    + "Hp, "
                    + "Shield, "
                    + "Mana, "
                    + "Dagger, "
                    + "WizardSpecialization)"
                    + "VALUES("
                    + randomId + ", "
                    + (char)34 + chosenClass + (char)34 + ", "
                    + (char)34 + name + (char)34 + ", "
                    + hp + ", "
                    + hasShield + ", "
                    + mana + ", "
                    + hasDagger + ", "
                    + (char)34 + wizardSpec + (char)34 +
                    ");";
            }
            else
            {
                sqlite_cmd.CommandText =
                    "INSERT INTO Characters("
                   + "Id, "
                   + "Class, "
                   + "Name, "
                   + "Hp, "
                   + "Shield, "
                   + "Mana, "
                   + "Dagger)"
                   + "VALUES("
                   + randomId + ", "
                   + (char)34 + chosenClass + (char)34 + ", "
                   + (char)34 + name + (char)34 + ", "
                   + hp + ", "
                   + hasShield + ", "
                   + mana + ", "
                   + hasDagger +
                   ");";

                sqlite_cmd.ExecuteNonQuery();
            }
        }

        public void Delete(string name)
        {

            SQLiteCommand sqlite_cmd;

            sqlite_cmd = this.connection.CreateCommand();

            sqlite_cmd.CommandText =
                    "DELETE FROM Characters " +
                    "WHERE"
                   + " Name"
                   + " = "
                   + (char)34 + name + (char)34
                   + "";

              sqlite_cmd.ExecuteNonQuery();
        }

        public void Update(Character selectedCharacter)
        {

            SQLiteCommand sqlite_cmd;

            sqlite_cmd = this.connection.CreateCommand();

            sqlite_cmd.CommandText =
                    "UPDATE Characters " +
                    "SET "
                   + "Hp = " + selectedCharacter.Hp + " "
                   + "WHERE Name = " + (char)34 + selectedCharacter.Name + (char)34
                   + ";";

            sqlite_cmd.ExecuteNonQuery();
        }

        public List<Character> GetCharacters()
        {
            List<Character> allCharacters = new List<Character>();

            SQLiteDataReader sqlite_datareader;

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = this.connection.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Characters";

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                if(sqlite_datareader.GetString(1) == "Warrior")
                {
                    allCharacters.Add(new Warrior(
                        sqlite_datareader.GetInt32(3),
                        sqlite_datareader.GetString(2),
                        false
                        ));
                }
                if (sqlite_datareader.GetString(1) == "Thief")
                {
                    allCharacters.Add(new Thief(
                        sqlite_datareader.GetInt32(3),
                        sqlite_datareader.GetString(2),
                        Convert.ToBoolean(sqlite_datareader.GetInt32(6)
                        )));
                }
                if (sqlite_datareader.GetString(1) == "Wizard")
                {
                    if (sqlite_datareader.GetString(7) == "Fire")
                    {
                        allCharacters.Add(new Wizard(
                            sqlite_datareader.GetInt32(3),
                            sqlite_datareader.GetString(2),
                            Character.WizardSpecialization.Fire
                            ));
                    }else if (sqlite_datareader.GetString(7) == "Frost")
                    {
                        allCharacters.Add(new Wizard(
                           sqlite_datareader.GetInt32(3),
                           sqlite_datareader.GetString(2),
                           Character.WizardSpecialization.Frost
                           ));
                    }
                    else
                    {
                        allCharacters.Add(new Wizard(
                           sqlite_datareader.GetInt32(3),
                           sqlite_datareader.GetString(2),
                           Character.WizardSpecialization.Arcane
                           ));
                    }
                }
            }
            return allCharacters;
        }

        public Character Get(string name)
        {
            Character character = null;

            SQLiteDataReader sqlite_datareader;

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = this.connection.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Characters WHERE Name = " + (char)34 + name + (char)34 + "";

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                if (sqlite_datareader.GetString(1) == "Warrior")
                {
                   character = new Warrior(
                        sqlite_datareader.GetInt32(3),
                        sqlite_datareader.GetString(2),
                        Convert.ToBoolean(sqlite_datareader.GetInt32(4)
                        ));
                }
                if (sqlite_datareader.GetString(1) == "Thief")
                {
                    character = new Thief(
                        sqlite_datareader.GetInt32(3),
                        sqlite_datareader.GetString(2),
                        Convert.ToBoolean(sqlite_datareader.GetInt32(6)
                        ));
                }
                if (sqlite_datareader.GetString(1) == "Wizard")
                {
                    if (sqlite_datareader.GetString(7) == "Fire")
                    {
                        character = new Wizard(
                            sqlite_datareader.GetInt32(3),
                            sqlite_datareader.GetString(2),
                            Character.WizardSpecialization.Fire
                            );
                    }
                    else if (sqlite_datareader.GetString(7) == "Frost")
                    {
                        character = new Wizard(
                           sqlite_datareader.GetInt32(3),
                           sqlite_datareader.GetString(2),
                           Character.WizardSpecialization.Frost
                           );
                    }
                    else
                    {
                        character = new Wizard(
                           sqlite_datareader.GetInt32(3),
                           sqlite_datareader.GetString(2),
                           Character.WizardSpecialization.Arcane
                           );
                    }
                }

            }
            return character;
        }
    }
    }
