﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
   public abstract class Character
    {
        public string Name { get; set; }
        public int Hp { get; set; }
        public int Armour { get; set; }


        public virtual void Attack()
        {
            Console.WriteLine("Character attacked");
        }

        public virtual void Move()
        {
            Console.WriteLine("Character moved");
        }

        public enum WizardSpecialization
        {
            Arcane,
            Fire,
            Frost
        }
    }
}

