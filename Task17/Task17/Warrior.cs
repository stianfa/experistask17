﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
   public class Warrior : Character
    {
        public const int warriorBaseArmour = 100;
        public bool HasShield { get; set; }

        public Warrior(int hp, string name, bool hasShield)
        {
            Hp = hp;
            Name = name;
            Armour = warriorBaseArmour;
            HasShield = hasShield;
        }

        public Warrior(int hp, string name, bool hasShield, int armour)
        {
            Hp = hp;
            Name = name;
            Armour = warriorBaseArmour + armour;
            HasShield = hasShield;
        }

        public void ShieldSmash(Object target)
        {
            if (HasShield)
            {
                Console.WriteLine($"{Name} casts shield smash!");
            }
            else
            {
                Console.WriteLine("No shield available, cant use shield smash");
            }
        }

        public override string ToString()
        {
            string summary = $"Class: Warrior\nName: {Name} \nHP: {Hp} \nArmour: {Armour} \n";
            summary += HasShield ? "Shield equipped" : "Shield not equipped";

            return summary;
        }
    }
}
