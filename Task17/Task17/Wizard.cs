﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
    public class Wizard : Character
    {
        public const int wizardBaseArmour = 0;
        public const int manaBase = 100;
        public bool HasShield { get; set; }
        public int Mana { get; set; }
        public WizardSpecialization Specialization { get; set; }

        public Wizard(int hp, string name, WizardSpecialization spes)
        {
            Hp = hp;
            Name = name;
            Armour = wizardBaseArmour;
            Mana = manaBase;
            Specialization = spes;
        }

        public Wizard(int hp, string name, WizardSpecialization spes, int armour)
        {
            Hp = hp;
            Name = name;
            Armour = wizardBaseArmour + armour;
            Mana = manaBase;
        }

        public void Polymorph(Object target)
        {
            if (Mana > 10)
            {
                Mana -= 10;
                Console.WriteLine("Target is now a sheep");
            }
            else
            {
                Console.WriteLine("Not enough mana to polymorph");
            }
        }

        public override string ToString()
        {
            string summary = $"Class: Wizard\nName: {Name} \nHP: {Hp} \nArmour: {Armour} \nMana: {Mana}\n";
            summary += "Specialization: ";

            switch (Specialization)
            {
                case WizardSpecialization.Arcane:
                    summary += "Arcane";
                    break;
                case WizardSpecialization.Fire:
                    summary += "Fire";
                    break;
                case WizardSpecialization.Frost:
                    summary += "Frost";
                    break;
                default:
                    break;
            }

            return summary;
        }
    }
}
