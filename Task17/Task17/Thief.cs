﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
    public class Thief : Character
    {
        public const int thiefBaseArmour = 50;
        public const int energyBase = 100;
        public bool HasShield { get; set; }
        public int Energy { get; set; }
        public bool HasDagger { get; set; }

        public Thief(int hp, string name, bool hasDagger)
        {
            Hp = hp;
            Name = name;
            Armour = thiefBaseArmour;
            Energy = energyBase;
            HasDagger = hasDagger;
        }

        public Thief(int hp, string name, bool hasDagger, int armour)
        {
            Hp = hp;
            Name = name;
            Armour = thiefBaseArmour + armour;
            Energy = energyBase;
            HasDagger = hasDagger;
        }

        public void Stab(Object target)
        {
            if (HasDagger && Energy > 10)
            {
                Energy -= 10;
                Console.WriteLine("stab stab stab stab");
            }
            else if (!HasDagger)
            {
                Console.WriteLine("No dagger available, nothing to stab with");
            }
            else
            {
                Console.WriteLine("Not enough energy to stab");
            }
        }


        public override string ToString()
        {
            string summary = $"Class: Thief\nName: {Name}\nHP: {Hp} \nArmour: {Armour} \nEnergy: {Energy}\n";
            summary += HasDagger ? "Dagger equipped" : "Dagger not equipped";

            return summary;
        }
    }
}

